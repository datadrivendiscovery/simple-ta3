#!/bin/bash -e

# Make sure directories do not have a trailing slash.
export D3MINPUTDIR="${D3MINPUTDIR%/}"
export D3MOUTPUTDIR="${D3MOUTPUTDIR%/}"

if [ "${D3MINPUTDIR#/}" == "$D3MINPUTDIR" ]; then
  echo "D3MINPUTDIR should start with /."
  exit 1
fi
if [ "${D3MOUTPUTDIR#/}" == "$D3MOUTPUTDIR" ]; then
  echo "D3MOUTPUTDIR should start with /."
  exit 1
fi

echo "{\"inputDir\": \"$D3MINPUTDIR\", \"outputDir\": \"$D3MOUTPUTDIR\"}" > /code/client/dist/config.json

# Configure nginx.
rm -f /etc/nginx/sites-enabled/*
cp /etc/nginx/sites-available/site.conf /etc/nginx/sites-enabled/site.conf

sed -i "s|__D3MINPUTDIR__|$D3MINPUTDIR|g" /etc/nginx/sites-enabled/site.conf
sed -i "s|__D3MOUTPUTDIR__|$D3MOUTPUTDIR|g" /etc/nginx/sites-enabled/site.conf
sed -i "s|__BRIDGE_HOST__|$BRIDGE_HOST|g" /etc/nginx/sites-enabled/site.conf
sed -i "s|__BRIDGE_PORT__|$BRIDGE_PORT|g" /etc/nginx/sites-enabled/site.conf

exec /usr/sbin/nginx
