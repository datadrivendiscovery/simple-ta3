FROM envoyproxy/envoy:v1.11.2

EXPOSE 8080/tcp
EXPOSE 9901/tcp

ENV TA2_HOST=localhost
ENV TA2_PORT=45042

COPY ./envoy.yml /etc/envoy/envoy.yaml
COPY ./envoy.sh /usr/local/bin/envoy.sh

ENTRYPOINT /usr/local/bin/envoy.sh
