describe('api', function () {
  // Time to load primitives, if necessary.
  const WARMUP_TIMEOUT = 5 * 60 * 1000;
  // A bit more time just in case.
  const EXTRA_TIMEOUT = 2 * 60 * 1000;
  // Time to score one fold.
  const ONE_FOLD_SCORE_TIMEOUT = 5 * 60 * 1000;
  // Time to return the response.
  const RESPONSE_TIMEOUT = 1 * 60 * 1000;

  afterEach(function () {
    // Wait a bit between tests.
    cy.wait(5 * 1000);
  });

  it('hello', function () {
    cy.visit('/');

    cy.get('.hello .do button').click();

    cy.get('.message.request h3').should('contain', 'HelloRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'HelloResponse');
    cy.get('.message.success, .message.error').should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');
  });

  it('list primitives', function () {
    cy.visit('/');

    cy.get('.list-primitives .do button').click();

    cy.get('.message.request h3').should('contain', 'ListPrimitivesRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'ListPrimitivesResponse');
    cy.get('.message.success, .message.error', {timeout: WARMUP_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');
  });

  it('run pipeline', function () {
    cy.visit('/');

    cy.get('.run .do button').click();

    // Search time bound is set to 5 minutes, plus 1 minute for fitting and 1 minute for producing.
    cy.get('.message.request h3').should('contain', 'SearchSolutionsRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'SearchSolutionsResponse');
    cy.get('.message.success, .message.error', {timeout: WARMUP_TIMEOUT + (5 + 1 + 1) * 60 * 1000 + EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');
  });

  it('search solutions', function () {
    cy.visit('/');

    cy.get('.search .do button').click();

    // Search time bound is set to 15 minutes.
    cy.get('.message.request h3').should('contain', 'SearchSolutionsRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'SearchSolutionsResponse');
    cy.get('.message.success, .message.error', {timeout: WARMUP_TIMEOUT + 15 * 60 * 1000 + EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');

    // Wait a bit.
    cy.wait(5 * 1000);

    cy.get('.search .clear button').click();

    cy.get('.describe .do button').click();

    cy.get('.message.request h3').should('contain', 'DescribeSolutionRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'DescribeSolutionResponse');
    cy.get('.message.success, .message.error', {timeout: EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');

    // Wait a bit.
    cy.wait(5 * 1000);

    cy.get('.describe .clear button').click();

    cy.get('.score-holdout .do button').click();

    cy.get('.message.request h3').should('contain', 'ScoreSolutionRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'ScoreSolutionResponse');
    cy.get('.message.success, .message.error', {timeout: ONE_FOLD_SCORE_TIMEOUT + EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');

    // Wait a bit.
    cy.wait(5 * 1000);

    cy.get('.score-holdout .clear button').click();

    cy.get('.score-kfold .do button').click();

    // 10 * ONE_FOLD_SCORE_TIMEOUT for 10 folds.
    cy.get('.message.request h3').should('contain', 'ScoreSolutionRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'ScoreSolutionResponse');
    cy.get('.message.success, .message.error', {timeout: 10 * ONE_FOLD_SCORE_TIMEOUT + EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');

    // Wait a bit.
    cy.wait(5 * 1000);

    cy.get('.score-kfold .clear button').click();

    cy.get('.export .do button').click();

    cy.get('.message.request h3').should('contain', 'SolutionExportRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'SolutionExportResponse');
    cy.get('.message.success, .message.error', {timeout: EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');

    // Wait a bit.
    cy.wait(5 * 1000);

    cy.get('.export .clear button').click();

    cy.get('.fit .do button').click();

    // The limit should be very similar to one fold.
    cy.get('.message.request h3').should('contain', 'FitSolutionRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'FitSolutionResponse');
    cy.get('.message.success, .message.error', {timeout: ONE_FOLD_SCORE_TIMEOUT + EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');

    // Wait a bit.
    cy.wait(5 * 1000);

    cy.get('.fit .clear button').click();

    cy.get('.produce .do button').click();

    // The limit should be very similar to one fold.
    cy.get('.message.request h3').should('contain', 'ProduceSolutionRequest');
    cy.get('.message.response h3', {timeout: RESPONSE_TIMEOUT}).should('contain', 'ProduceSolutionResponse');
    cy.get('.message.success, .message.error', {timeout: ONE_FOLD_SCORE_TIMEOUT + EXTRA_TIMEOUT}).should('be.visible').first().scrollIntoView();
    cy.get('.message.error').should('not.exist');

    // Wait a bit.
    cy.wait(5 * 1000);

    cy.get('.produce .clear button').click();
  });
});
