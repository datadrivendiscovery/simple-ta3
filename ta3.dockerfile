FROM tozd/base:ubuntu-bionic

RUN \
 apt-get update -q -q && \
 apt-get install --yes --force-yes build-essential curl unzip autoconf libtool nginx-full git && \
 curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
 apt-get install --yes --force-yes nodejs && \
 echo "daemon off;" >> /etc/nginx/nginx.conf && \
 ln -sf /dev/stdout /var/log/nginx/access.log && \
 ln -sf /dev/stderr /var/log/nginx/error.log && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./grpc-web /code/grpc-web

RUN \
 cd /code/grpc-web/third_party/grpc/third_party/protobuf && \
 ./autogen.sh && ./configure && make -j8 && \
 make install && \
 cd /code/grpc-web && \
 make install-plugin && \
 echo /usr/local/lib > /etc/ld.so.conf.d/protoc.conf && \
 ldconfig

COPY ./d3m-automl-rpc /code/d3m-automl-rpc

COPY ./client /code/client

RUN \
 cd /code && \
 mkdir -p client/lib && \
 cd d3m-automl-rpc && \
 for file in *.proto; do \
   echo "$file" ; \
   protoc -I=. "$file" --js_out=import_style=commonjs:../client/lib --grpc-web_out=import_style=commonjs,mode=grpcwebtext:../client/lib ; \
 done && \
 cd /code/client && \
 npm install && \
 npx webpack src/index.js

COPY ./ta3.sh /usr/local/bin/ta3.sh

ENV BRIDGE_HOST=localhost
ENV BRIDGE_PORT=8080
ENV D3MINPUTDIR=/input
ENV D3MOUTPUTDIR=/output
ENV D3MRUN=ta2ta3

COPY ./site.conf /etc/nginx/sites-available/site.conf
COPY ./proxy.conf /etc/nginx/conf.d/proxy.conf

EXPOSE 80/tcp

ENTRYPOINT /usr/local/bin/ta3.sh
