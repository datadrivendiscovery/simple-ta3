FROM registry.gitlab.com/datadrivendiscovery/images/testing:ubuntu-bionic-python36

ENV TA2_HOST=localhost
ENV TA2_PORT=45042
ENV WAIT_TIMEOUT=

WORKDIR /code

COPY ./d3m-automl-rpc /code

RUN \
 for file in *.proto; do \
   echo "$file" ; \
   python3 -m grpc_tools.protoc -I . -I /protoc3/include --python_out=. --grpc_python_out=. "$file" ; \
 done

COPY ./wait.py /code/wait.py

ENTRYPOINT /code/wait.py
