#!/bin/bash -e

# Populate configuration based on environment variables.
cp /etc/envoy/envoy.yaml /etc/envoy/envoy-real.yaml
sed -i "s|__TA2_HOST__|$TA2_HOST|g" /etc/envoy/envoy-real.yaml
sed -i "s|__TA2_PORT__|$TA2_PORT|g" /etc/envoy/envoy-real.yaml

exec /usr/local/bin/envoy -c /etc/envoy/envoy-real.yaml
