# Simple TA3

A simple TA3 system which tried to fully exercise a TA3-TA2 API.
Its UI is simple and consist of two columns. On the left they are
buttons which start a predefined set of calls. On the right you see
GRPC messages which are being sent and received. TA3 tries to also
do some basic protocol validation.

## Running

To run it you can use [available Docker images](https://gitlab.com/datadrivendiscovery/simple-ta3/container_registry):
 * `registry.gitlab.com/datadrivendiscovery/simple-ta3/envoy:latest` is a GRPC bridge used
   to communicate between the client-side and a TA2 system.
 * `registry.gitlab.com/datadrivendiscovery/simple-ta3/simple-ta3:latest` serves the
   HTML and JavaScript for the client-side.

```
$ HOST_IP=$(hostname -I | awk '{print $1}')
$ docker run --rm --name envoy -d \
  -e TA2_HOST=$HOST_IP -e TA2_PORT=45042 \
  -p 8080:8080 \
  registry.gitlab.com/datadrivendiscovery/simple-ta3/envoy:latest
$ docker run --rm --name ta3 -d \
  -v /datasets/d3m-datasets:/input -v /datasets/output:/output \
  -e BRIDGE_HOST=$HOST_IP -e BRIDGE_PORT=8080 \
  -e D3MINPUTDIR=/input -e D3MOUTPUTDIR=/output -e D3MRUN=ta2ta3 \
  -p 80:80 \
  registry.gitlab.com/datadrivendiscovery/simple-ta3/simple-ta3:latest
```

Envoy bridge has to know the address of the TA2 system and exposes
(by default) port 8080. Simple TA3 system connects to the bridge and
exposes port 80 to which the browser should connect. TA2 and TA3 systems
should share input and output directories (output should be read-write)
and have the same values set for `D3MINPUTDIR` and `D3MOUTPUTDIR`.

## Testing

Test suite is packed as a [Docker image](https://gitlab.com/datadrivendiscovery/simple-ta3/container_registry)
as well: `registry.gitlab.com/datadrivendiscovery/simple-ta3/tests:latest`.
You can run it with:

```
$ HOST_IP=$(hostname -I | awk '{print $1}')
$ docker run --rm --name tests -e "TA3_URL=http://$HOST_IP" \
  registry.gitlab.com/datadrivendiscovery/simple-ta3/tests:latest \
  npm run test
```

There is a volume available into which tests store a video
recording of the testing session, any screenshots, and if enabled,
results in JUnit/xUnit XML format. It is available at
`/tests/cypress/results`.

You can also run tests locally, by installing and running:

```
$ cd tests
$ npm install
$ npm run open
```

This will open [Cypress](https://www.cypress.io/) user interface
which makes it easier to interact with tests and understand failures.
Tests try to connect to `http://localhost` by default and you can change
this by setting `TA3_URL` environment variable:

```
TA3_URL=http://example.com npm run open
```

## Wait for TA2 Docker image

There is also a [Docker image available](https://gitlab.com/datadrivendiscovery/simple-ta3/container_registry)
which can be used for
[Kubernetes init container](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/)
to make the pod wait until TA2 is ready (successfully responds to `Hello` call):
`registry.gitlab.com/datadrivendiscovery/simple-ta3/wait:latest`.
It can be configured with the following environment variables:

* `TA2_HOST` (default: `localhost`): Hostname to which to try to connect.
* `TA2_PORT` (default: `45042`): Port to which to try to connect.
* `WAIT_TIMEOUT` (optional): In seconds, if unable to connect after this time, raise an exception.
