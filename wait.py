#!/usr/bin/env python3

# Waits until the "Hello" call succeeds, then exits.
#
# Uses three environment variables:
#  - TA2_HOST (default: localhost): Hostname to which to try to connect.
#  - TA2_PORT (default: 45042): Port to which to try to connect.
#  - WAIT_TIMEOUT (optional): In seconds, if unable to connect after this time, raise an exception.

import datetime
import os
import time

import grpc

import core_pb2
import core_pb2_grpc


start = time.monotonic()
while True:
    try:
        with grpc.insecure_channel('{hostname}:{port}'.format(hostname=os.environ.get('TA2_HOST', 'localhost'), port=os.environ.get('TA2_PORT', '45042'))) as channel:
            stub = core_pb2_grpc.CoreStub(channel)
            request = core_pb2.HelloRequest()
            stub.Hello(request)
            print(datetime.datetime.now(), "Success.")
            break
    except grpc.RpcError as error:
        print(datetime.datetime.now(), "Hello failed: {error}".format(error=error))

        if os.environ.get('WAIT_TIMEOUT', None) and (time.monotonic() - start) > int(os.environ['WAIT_TIMEOUT']):
            raise TimeoutError("Waiting timeout.") from None

        time.sleep(1)
