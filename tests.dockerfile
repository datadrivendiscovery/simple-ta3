FROM tozd/base:ubuntu-bionic

ENV TA3_URL=http://localhost

RUN \
 apt-get update -q -q && \
 apt-get install --yes --force-yes build-essential curl unzip patch && \
 apt-get install --yes --force-yes xvfb libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 && \
 curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
 apt-get install --yes --force-yes nodejs && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./tests /tests

WORKDIR /tests

RUN npm install

COPY ./patches /patches

RUN for patch in /patches/*; do patch --directory=/ --prefix=/patches/ -p0 --force "--input=/$patch" || exit 1; done && \
 rm -rf /patches

VOLUME /tests/cypress/results
