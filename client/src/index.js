import 'babel-polyfill';
import Vue from 'vue';

import {library} from '@fortawesome/fontawesome-svg-core';
import {faArrowUp, faArrowDown} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';

import App from './App';
import {CorePromiseClient} from '../lib/core_grpc_web_pb.js';

library.add(faArrowUp);
library.add(faArrowDown);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;
Vue.config.devtools = false;

fetch('/config.json').then((response) => {
  if (!response.ok) {
    throw new Error("Could not fetch config JSON.");
  }

  return response.json();
}).then((config) => {
  Vue.prototype.$config = config;
  Vue.prototype.$client = new CorePromiseClient(window.location.origin);

  new Vue({
    el: '#app',
    render: (createElement) => {
      return createElement(App);
    },
  });
});
